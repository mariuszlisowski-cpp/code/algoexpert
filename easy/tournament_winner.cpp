#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <string>
#include <vector>

// O(n) time | O(keys) space
std::string tournamentWinner(std::vector<std::vector<std::string>> competitions,
                             std::vector<int> results)
{
    std::unordered_map<std::string, int> scores;        //  hashmap (keys)
    for (size_t i = 0; i < results.size(); ++i) {
        if (results[i]) {
            ++scores[competitions[i][0]];
        } else {
            ++scores[competitions[i][1]];
        }
    }

    return std::max_element(scores.begin(), scores.end(),
                                [](const std::pair<std::string, int>& lhs,
                                   const std::pair<std::string, int>& rhs)
                                {
                                    return lhs.second < rhs.second;
                                })->first;
}

int main() {
    std::vector<std::vector<std::string>> competitions{
        {"HTML", "C#"},                                 // awayTeam won: C#     (0)
        {"C#", "Python"},                               // awayTeam won: Python (0)
        {"Python", "HTML"}                              // homeTeam won: Python (1)
    };
    std::vector<int> results{0, 0, 1};                  // 1 - homeTeam won

    std::cout << tournamentWinner(competitions, results) << std::endl;
    

    return 0;
}
