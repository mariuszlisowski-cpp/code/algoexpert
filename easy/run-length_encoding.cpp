#include <cassert>
#include <iostream>
#include <string>

std::string runLengthEncoding(std::string str) {
    if (str.empty()) {
        return str;
    }
    std::string output;
    for (int i{}; i < str.size();) {
        int counter { 1 };
        char current{ str[i] };
        while (current == str[++i] && counter < 9) {
            ++counter;
        }
        output += std::to_string(counter) + current;
    }

    return output;
}

int main() {
    auto input{ "AAAAAAAAAAAAABBCCCCDD" };
    auto expected{ "9A4A2B4C2D" };
    auto actual{ runLengthEncoding(input) };

    assert(actual == expected);

    return 0;
}
