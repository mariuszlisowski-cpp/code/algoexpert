#include <iostream>
#include <string>

bool helper(std::string str, int i);

// O(n) time | O(n) space
bool isPalindrome(std::string str) { return helper(str, 0); }

bool helper(std::string str, int i) {
    int j = str.length() - 1 - i;
    return i >= j ? true : str[i] == str[j] && helper(str, i + 1);
}

int main() {
    std::string str{""};

    std::cout << (isPalindrome(str) ? "palindrome" : "NOT palindrome")
              << std::endl;

    return 0;
}
