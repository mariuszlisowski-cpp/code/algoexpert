#include <cassert>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <unordered_map>
#include <string>
#include <vector>

/*  Write a function that takes in a non-empty list of non-empty strings and
    returns a list of characters that are common to all strings in the list,
    ignoring multiplicity.

    Note that the strings are not guaranteed to only contain alphanumeric characters.
    The list you return can be in any order.
*/

// O(n * m) time - where n is the number of strings and m is length of the longest string
// O(c) space c is the total number of unique characters in all strings
std::vector<std::string> common_characters(std::vector<std::string> strings) {
    if (strings.size() == 0) {
        return {};
    }
    
    std::unordered_map<char, std::size_t> occurences;
    for (auto& str : strings) {
        std::sort(str.begin(), str.end());                                          // sort before removing duplicates
        str.erase(std::unique(str.begin(), str.end()), str.end());                  // as 'unique' removes ONLY consecutive values
        for (const auto ch : str) {
            ++occurences[ch];
        }
    }

    /* output of single character occurences */
    for (auto [ch, occurence] : occurences) {
        std::cout << ch << ':' << occurence << ' ';
    }

    std::vector<std::string> results;
    std::for_each(occurences.begin(), occurences.end(),
                  [&](auto& pair) {
                      if (strings.size() == pair.second) {                          // char seen in all strings (size of vector)
                          results.push_back(std::string{pair.first});               // convert char to string
                      }
                  });

    /* list of characters that are common to all strings */
    std::copy(results.begin(), results.end(),
              std::ostream_iterator<std::string>(std::cout, " "));

    return results;
}

int main() {
    std::vector<std::string> strings{"abc", "bcd", "cbaccd"};
    std::vector<std::string> expected{"b", "c"};                                    // the characters could be ordered differently

    common_characters(strings);

    return 0;
}
