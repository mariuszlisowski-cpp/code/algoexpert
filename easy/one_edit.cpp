/* 
You're given two strings stringOne and stringTwo.
Write a function that determines if these two strings can be made equal using only one edit.

There are 3 possible edits:
    Replace: One character in one string is swapped for a different character.
    Add: One character is added at any index in one string.
    Remove: One character is removed at any index in one string.

Note that both strings will contain at least one character. If the strings are the same, your function should return true.

Sample Input:
stringOne = "hello"
stringTwo = "hollo"

Sample Output:
True // A single replace at index 1 of either string can make the strings equal
 */
#include <cassert>
#include <cstdlib>
#include <string>

bool oneEdit(std::string stringOne, std::string stringTwo) {
    if (stringOne == stringTwo) {
        return true; // strings are already equal
    }
  
    if (std::abs(static_cast<int>(stringOne.size()) - static_cast<int>(stringTwo.size())) > 1) {
      return false; // more than one edit is required
    }

    int i = 0, j = 0;
    bool foundDiff = false;

    while (i < stringOne.size() && j < stringTwo.size()) {
        if (stringOne[i] != stringTwo[j]) {
            if (foundDiff) {
                return false; // more than one edit is required
            }
            foundDiff = true;

            if (stringOne.size() == stringTwo.size()) { // replace
                i++;
                j++;
            } else if (stringOne.size() < stringTwo.size()) { // add
                j++;
            } else { // remove
                i++;
            }
        } else {
            i++;
            j++;
        }
    }

    return true;
}

int main() {
    auto stringOne = "hello";
    auto stringTwo = "hollo";                                            // replace 'o' with 'e'

    // auto stringOne = "hello";
    // auto stringTwo = "helo";                                             // add 'l'

    // auto stringOne = "a";
    // auto stringTwo = "ab";

    auto expected = true;
    auto actual = oneEdit(stringOne, stringTwo);
    assert(expected == actual);

    return 0;
}
