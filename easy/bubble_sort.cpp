#include <assert.h>
#include <iostream>
#include <utility>
#include <vector>

// average O(n^2) time | O(1) space
std::vector<int> bubbleSort(std::vector<int> array) {
    bool isSorted;
    size_t size{ array.size() };
    do {
        --size;                                         // the last is the highest (no need to compare it)
        isSorted = true;                                // let's assume array is already sorted
        for (size_t i{}; i < size; ++i) {               // size already decreased to read the last element safely
            if (array[i] > array[i + 1]) {              // last read here
                std::swap(array[i], array[i + 1]);
                isSorted = false;                       // there's been a swap thus it hadn't been sorted yet
            }
        }
    } while (!isSorted);

    return array;
}

int main() {
    std::vector<int> unsorted{ 8, 5, 2, 9, 5, 6, 3 };
    std::vector<int> sorted{ 2, 3, 5, 5, 6, 8, 9 };

    assert(bubbleSort(unsorted) ==  sorted);

    return 0;
}
