#include <iostream>
#include <memory>

/* time O(n) | space O(1) same amount of space used */
int getNthFib(int n) {
    int first{};
    int second{1};
    int third;
    while (--n) {
        third = first + second;
        first = second;
        second = third;
    }

    return first;
}

int main() {
    const int nth_fibonacci = 10;
    std::cout << nth_fibonacci << "th Fibonacci number: " << getNthFib(nth_fibonacci) << std::endl;

    return 0;
}

/* 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 seq
   1  2  3  4  5  6  7   8   9  10 th */
