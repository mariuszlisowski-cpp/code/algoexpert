#include <cassert>
#include <string>
#include <vector>

class Node {
public:
    std::string name;
    std::vector<Node*> children;

    Node(std::string str) { name = str; }

    // O(v+e) time | O(v) space, where v-vertices (nodes), e-edges (lines)
    std::vector<std::string> depthFirstSearch(std::vector<std::string>* array) {
        array->push_back(this->name);
        for (const auto& child : this->children) {
            child->depthFirstSearch(array);
        }

        return *array;
    }

    Node* addChild(std::string name) {
        Node* child = new Node(name);
        children.push_back(child);
        return this;
    }
};

int main() {
    Node graph("A");
    graph.addChild("B")->addChild("C")->addChild("D");
    graph.children[0]->addChild("E")->addChild("F");
    graph.children[2]->addChild("G")->addChild("H");
    graph.children[0]->children[1]->addChild("I")->addChild("J");
    graph.children[2]->children[0]->addChild("K");

    std::vector<std::string> expected{ "A", "B", "E", "F", "I", "J",
                                       "C", "D", "G", "K", "H" };
    std::vector<std::string> inputArray{};

    assert(graph.depthFirstSearch(&inputArray) == expected);

    return 0;
}
