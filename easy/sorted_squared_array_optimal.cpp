#include <algorithm>
#include <cassert>
#include <vector>

// optimal solution
// O(n) time | O(n) space, n - length of the input array
std::vector<int> sortedSquaredArray(std::vector<int> array) {
    std::vector<int> squared(array.size());
    auto first{ array.begin() };
    auto last{ array.end() - 1};
    for (int i{ (int)squared.size() - 1 }; i >= 0; --i) {
        if (std::abs(*first) > std::abs(*last)) {
            squared[i] = *first * *first;
            ++first;
        } else {
            squared[i] = *last * *last;
            --last;
        }
    }

    return squared;
}

int main() {
    std::vector<int> input = {-7, -5, -4, 3, 6, 8, 9};
    std::vector<int> expected = {9, 16, 25, 36, 49, 64, 81};
    auto actual = sortedSquaredArray(input);
    
    assert(expected == actual);

    return 0;
}
