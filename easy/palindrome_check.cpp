#include <iostream>
#include <string>

// O(n) time | O(1) space (BEST!)
bool isPalindrome(std::string str) {
    size_t left_idx{};
    size_t right_idx{str.size() - 1};
    while (left_idx < right_idx) {
        if (str[left_idx] != str[right_idx]) {
            return false;
        }
        ++left_idx;
        --right_idx;
    }

    return true;
}

int main() {
    std::string str{""};

    std::cout << (isPalindrome(str) ? "palindrome" : "NOT palindrome") << std::endl;

    return 0;
}
