#include <cstddef>
#include <iostream>
#include <memory>
#include <vector>

class BinaryTree {
  public:
    int value;
    BinaryTree* left;
    BinaryTree* right;

    BinaryTree(int value) {
        this->value = value;
        left = nullptr;
        right = nullptr;
    }
};

/* time O(n) nodes | space O(h) tree height */
int nodeDepths(BinaryTree *root, int depth = 0) {
    return root ? depth + nodeDepths(root->left, depth + 1) 
                        + nodeDepths(root->right, depth + 1) : 0;
}

int main() {
    BinaryTree* root = new BinaryTree(1);
    root->left = new BinaryTree(2);
    root->left->left = new BinaryTree(4);
    root->left->left->left = new BinaryTree(8);
    root->left->left->right = new BinaryTree(9);
    root->left->right = new BinaryTree(5);
    root->right = new BinaryTree(3);
    root->right->left = new BinaryTree(6);
    root->right->right = new BinaryTree(7);
    int actual = nodeDepths(root);
    assert(actual == 16);

    return 0;
}
