/* You're given a Linked List with at least one node. Write a function that returns the middle node of the Linked List.
   If there are two middle nodes (i.e. an even length list), your function should return the second of these nodes.

   Each LinkedList node has an integer value as well as a next node pointing to the next node in the list or to None/null
   if it's the tail of the list. 
 */
#include <iostream>
#include <vector>

class LinkedList {
public:
    int value;
    LinkedList* next{};

    LinkedList(int value): value(value) {}
};

LinkedList* middleNode(LinkedList* linkedList) {
    auto count = 0u;
    LinkedList* current = linkedList;
    std::vector<decltype(linkedList)> vec;
    while (current) {
        ++count;
        vec.push_back(current);
        current = current->next;
    }

    return vec[count / 2u];
}

void traverse_linked_list(LinkedList* head) {
    if (!head) { return; }
    LinkedList* current = head;
    while (current) {
        std::cout << current->value << "->";
        current = current->next;
    }
    std::cout << "null";
}

int main() {
    LinkedList ll0{2};
    LinkedList ll1{7};
    LinkedList ll2{3};
    LinkedList ll3{5};                                                      // even length
    // LinkedList ll4{1};                                                   // odd length
    ll0.next = &ll1;
    ll1.next = &ll2;
    ll2.next = &ll3;
    // ll3.next = &ll4;

    traverse_linked_list(&ll0);
    auto result = middleNode(&ll0);
    std::cout << std::endl << "Middle node: " << (result->value) << std::endl;

    return 0;
}
