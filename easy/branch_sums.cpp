#include <cstddef>
#include <iostream>
#include <memory>
#include <vector>

class BinaryTree {
  public:
    int value;
    BinaryTree* left;
    BinaryTree* right;

    BinaryTree(int value) {
        this->value = value;
        left = nullptr;
        right = nullptr;
    }
};

class TestBinaryTree : public BinaryTree {
  public:
    TestBinaryTree(int value) : BinaryTree(value){};

    BinaryTree* insert(std::vector<int> values, int i = 0) {
        if (i >= values.size())
            return nullptr;
        std::vector<BinaryTree*> queue = {this};
        while (queue.size() > 0) {
            BinaryTree* current = queue[0];
            queue.erase(queue.begin());
            if (current->left == nullptr) {
                current->left = new BinaryTree(values[i]);
                break;
            }
            queue.push_back(current->left);
            if (current->right == nullptr) {
                current->right = new BinaryTree(values[i]);
                break;
            }
            queue.push_back(current->right);
        }
        insert(values, i + 1);
        return this;
    }
};

/* time O(n) all nodes visited | space O(n) */
std::vector<int> branchSums(BinaryTree* root,
                            const std::unique_ptr<std::vector<int>>& result = std::make_unique<std::vector<int>>(),
                            int addend = 0)
{ 
    if (root) {
        int sum{addend};
        sum += root->value;
        if (root->left) { 
            branchSums(root->left, result, sum);
        }
        if (root->right) {
            branchSums(root->right, result, sum);
        }
        if (!root->left && !root->right) {
            result->push_back(sum);
        }
    }

    return *result.get();
}

int main() {

    TestBinaryTree* tree = new TestBinaryTree(1);
    tree->insert({2, 3, 4, 5, 6, 7, 8, 9, 10});
    std::vector<int> expected = {15, 16, 18, 10, 11};
    assert(branchSums(tree) == expected);

    tree = new TestBinaryTree(1);
    expected = {1};
    assert(branchSums(tree) == expected);
    
    tree = nullptr;
    expected = {};
    assert(branchSums(tree) == expected);

    return 0;
}
