#include <array>
#include <algorithm>
#include <unordered_map>
#include <string>
#include <vector>
#include <utility>
#include <iostream>

/* You recently started freelance software development and have been offered a variety of job opportunities.
   Each job has a deadline, meaning there is no value in completing the work after the deadline
   Additionally, each job has an associated payment representing the profit for completing that job.
   Given this information, write a function that returns the MAXIMUM PROFIT that can be obtained in a 7-day period.

  Each job will take 1 full day to complete, and the deadline will be given as the number of days left to complete the job.
  For example, if a job has a deadline of 1, then it can only be completed if it is the first job worked on.
  If a job has a deadline of 2, then it could be started on the first or second day.

  Note: There is no requirement to complete all of the jobs. Only one job can be worked on at a time,
        meaning that in some scenarios it will be impossible to complete them all.
*/

const auto MAX_PERIOD{10};
const auto WEEK_PERIOD{7};

void print_period(const std::array<bool, MAX_PERIOD> &period);

/* time O(n x log(n) | space O(1) | n is number of jobs */
auto optimalFreelancing(std::vector<std::unordered_map<std::string, int>> jobs) {
    if (jobs.empty()) {
        return 0;
    }

    /* log(n) time */
    std::sort(jobs.begin(),
              jobs.end(),
              [](const auto& lhs, const auto& rhs) {
                  return lhs.at("payment") > rhs.at("payment");
              });

    auto profit{0};
    std::array<bool, MAX_PERIOD> period{};
    for (const auto &job : jobs) {
        // VERBOSE:
        // std::cout << "deadline: "<< job.at("deadline") << " | payment: " << job.at("payment") << std::endl;

        /* not affecting time complexity as is constant */
        for (auto deadline = std::min(job.at("deadline"), WEEK_PERIOD) - 1; deadline >= 0; deadline--) {
            if (!period.at(deadline)) {
                // VERBOSE:
                // std::cout << "> job accepted" << std::endl;

                profit += job.at("payment");
                period.at(deadline) = true;
                // VERBOSE:
                // print_period(period);

                break;
            }
        }
    }

    return profit;
}

int main() {
    {
    std::vector<std::unordered_map<std::string, int>> jobs = {
        {{"deadline", 1}, {"payment", 1}},  // 1 x
        {{"deadline", 2}, {"payment", 2}},  // 2
        {{"deadline", 2}, {"payment", 2}},  // 3
        {{"deadline", 7}, {"payment", 1}},  // 4
        {{"deadline", 4}, {"payment", 3}},  // 5
        {{"deadline", 4}, {"payment", 5}},  // 6
        {{"deadline", 3}, {"payment", 1}}   // 7 x
    };
    // order    2 3 5 6 1 7 4
    // day      1 2 3 4 5 6 7
    // payment  2 2 3 5 x x 1   = 13 maximum profit
    auto profit = optimalFreelancing(jobs);
    std::cout << profit << std::endl;
    }
    {
    std::vector<std::unordered_map<std::string, int>> jobs = {
        {{"deadline", 1}, {"payment", 1}},
        {{"deadline", 2}, {"payment", 1}},
        {{"deadline", 3}, {"payment", 1}},
        {{"deadline", 4}, {"payment", 1}},
        {{"deadline", 5}, {"payment", 1}},
        {{"deadline", 6}, {"payment", 1}},
        {{"deadline", 7}, {"payment", 1}},
        {{"deadline", 8}, {"payment", 1}},
        {{"deadline", 9}, {"payment", 1}},
        {{"deadline", 10}, {"payment", 1}}
    };
    // payment  1 1 1 1 1 1 1   = 7 maximum profit (as period is 7 days)
    auto profit = optimalFreelancing(jobs);
    std::cout << profit << std::endl;
    }

    return 0;
}

void print_period(const std::array<bool, MAX_PERIOD>& period) {
    for (const auto& day: period) {
        std::cout << day << " ";
    }
    std::cout << std::endl << std::endl;
}
