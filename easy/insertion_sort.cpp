#include <assert.h>
#include <iostream>
#include <utility>
#include <vector>

void insert(int value, std::vector<int>& sorted ) {
    sorted.push_back(value);
    for (size_t i{sorted.size() - 1}; i > 0; --i) {
        if (sorted[i] < sorted[i - 1]) {
            std::swap(sorted[i], sorted[i - 1]);
        }
    }
}

// average O(n^2) time | O(1) space
std::vector<int> insertionSort(std::vector<int> array) {
    if (array.empty()) {
        return {};
    }
    std::vector<int> sorted;
    sorted.reserve(array.size());
    for (size_t i{}; i < array.size(); ++i) {
        insert(array[i], sorted);
    }

    return sorted;
}

int main() {
    std::vector<int> unsorted{ 8, 5, 2, 9, 5, 6, 3 };
    std::vector<int> sorted{ 2, 3, 5, 5, 6, 8, 9 };

    assert(insertionSort(unsorted) == sorted);

    return 0;
}
