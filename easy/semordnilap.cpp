/* Write a function that takes in a list of unique strings and returns a list of semordnilap pairs. A semordnilap pair is
   defined as a set of different strings where the reverse of one word is the same as the forward version of the other.
   For example the words "diaper" and "repaid" are a semordnilap pair, as are the words "palindromes" and "semordnilap".
   The order of the returned pairs and the order of the strings within each pair does not matter.
 */

#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>

std::vector<std::vector<std::string>> semordnilap(std::vector<std::string> words) {
    std::vector<std::vector<std::string>> semordnilaps;
    std::unordered_set<std::string> uniques;
    for (const auto& word: words) {
        auto reversed = word;
        std::reverse(reversed.begin(), reversed.end());
        if (uniques.find(reversed) != uniques.end()) {
            semordnilaps.push_back({word, reversed});
        }
        uniques.insert(word);
    }

    return semordnilaps;
}

void print_dimensional_array(std::vector<std::vector<std::string>>& array) {
    for (auto& row: array) {
        std::cout << "{ ";
        for (auto &col : row)
        {
            std::cout << col << " ";
        }
        std::cout << "}" << std::endl;
    }
}

int main() {
    {
    std::vector<std::string> words {"desserts", "stressed", "hello"};
    auto semordnilaps = semordnilap(words);
    print_dimensional_array(semordnilaps);
    }

    std::cout << std::string(21, '-') << std::endl;

    {
    std::vector<std::string> words { "diaper", "abc", "test", "cba", "repaid" };
    auto semordnilaps = semordnilap(words);
    print_dimensional_array(semordnilaps);
    }

    return 0;
}
