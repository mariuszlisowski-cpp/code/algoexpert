#include <cassert>
#include <iostream>
#include <vector>

// O(log(n)) time | O(log(n)) space
int binary_search(const std::vector<int>& array, int target, int begin, int end) {
	if (begin < end) {
		int mid = (begin + end) / 2;
        // verbose
        std::cout << begin << ' ' << end << ' ' << mid << std::endl;
		if (target == array[mid]) {
            return mid;
        } else if (target < array[mid]) {
            return binary_search(array, target, begin, mid);
        } else {
            return binary_search(array, target, mid + 1, end);
        }
	}

	return -1;
}

int binarySearch(std::vector<int> array, int target) {
    return binary_search(array, target, 0, array.size());
}

int main() {
    std::vector<int> v{ 0, 1, 21, 33, 45, 45, 61, 71, 72, 73 };
    int target{ 72 };                                                       // whether is contained in an array
    int result{ 8 };                                                        // index of contained integer

    assert(binarySearch(v, target) == result);                              // needs 3 iterations

    return 0;
}
