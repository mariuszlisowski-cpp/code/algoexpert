#include <algorithm>
#include <iostream>
#include <limits>
#include <memory>

class BST {
  public:
    int value;
    BST* left;
    BST* right;

    BST(int value) {
        this->value = value;
        left = nullptr;
        right = nullptr;
    }
};

/* ava: time O(log n)  | space O(log n)
   worst: time O(n) | space O(n) */
int findClosestValueInBstHelper(BST* tree, int target, int closest) {
    if (std::abs(target - closest) > std::abs(target - tree->value)) {
        closest = tree->value;
    }

    if (tree->value < target && tree->right) {
        return findClosestValueInBstHelper(tree->right, target, closest);
    } else if (tree->left) {
        return findClosestValueInBstHelper(tree->left, target, closest);
    } else {
        return closest;
    }
}

int findClosestValueInBst(BST* tree, int target) {
    return findClosestValueInBstHelper(tree, target, tree->value);
}

int main() {
    BST* root = new BST(10);
    root->left = new BST(5);
    root->left->left = new BST(2);
    root->left->left->left = new BST(1);
    root->left->right = new BST(5);
    root->right = new BST(15);
    root->right->left = new BST(13);
    root->right->left->right = new BST(14);
    root->right->right = new BST(22);

    int expected = 13;
    int actual = findClosestValueInBst(root, 12);
    assert(expected == actual);

    return 0;
}
