#include <iostream>
#include <unordered_set>
#include <vector>

/*  time    O(n) one traveral
    space   O(n) adding to hash table
*/
std::vector<int> twoNumberSum(const std::vector<int>& array, int targetSum) {
    std::vector<int> result;
    std::unordered_set<int> hash_table;
    
    /*  x + y = targetSum, eg. 11 - 1 = 10
        y = targetSum - x, eg. 11 = 10 - (-1) */
    for (auto x : array) {
        int y = targetSum - x;
        auto it = hash_table.find(y);
        if (it != hash_table.end()) {
            result.push_back(x);
            result.push_back(*it);
            break;
        } else {
            hash_table.insert(x);
        }
    }    
  
    return result;
}

int main() {
    std::vector<int> res = twoNumberSum({3, 5, -4, 8, 11, 1, -1, 6}, 10); // 11, -1

    for (auto el : res) {
        std::cout << el << std::endl;
    }

    return 0;
}
