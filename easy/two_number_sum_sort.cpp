#include <algorithm>
#include <cstdio>
#include <iostream>
#include <vector>

void print_vector(const std::vector<int>& vec) {
    for (auto el : vec) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

/*  time    O(n * log n)
    space   O(1)
*/
std::vector<int> twoNumberSum(const std::vector<int>& array, int targetSum) {
    std::vector<int> result;

    // time O(log n) if quicksort
    std::sort(array.begin(), array.end()); // {-4, -1, 1, 3, 5, 6, 8, 11}

    // time O(n)
    auto left = array.begin();
    auto right = array.end() - 1;
    while (left != right) {
        int sum = *left + * right;
        if (sum == targetSum) {
            result.emplace_back(*left);
            result.emplace_back(*right);
            break;                              // found!
        }
        if (sum < targetSum) {
            ++left;                             // next must be bigger (because array is sorted)
        } else {
            --right;                            // prev must be smaller
        }
    }

    return result;
}

int main() {
    std::vector<int> resultA = twoNumberSum({3, 5, -4, 8, 11, 1, -1, 6}, 10); // -1, 11
    std::vector<int> resultB = twoNumberSum({3, 5, -4, 8, 11, 1, -1, 6}, 13); // 5, 8

    print_vector(resultA);
    print_vector(resultB);
    
    return 0;
}
