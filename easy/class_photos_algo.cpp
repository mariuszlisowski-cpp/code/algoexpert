#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

void print(std::vector<int> v) {
    for (auto el : v) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

// O(nlog(n)) time | O(1) space - where n is the number of students
bool classPhotos(std::vector<int> redShirtHeights, std::vector<int> blueShirtHeights) {
    std::sort(std::begin(redShirtHeights), std::end(redShirtHeights));
    std::sort(std::begin(blueShirtHeights), std::end(blueShirtHeights));

    /* verbose */
    print(redShirtHeights);
    print(blueShirtHeights);

    std::string shirtColorInFirstRow =
        redShirtHeights[0] < blueShirtHeights[0] ? "RED" : "BLUE";

    for (int i = 0; i < redShirtHeights.size(); i++) {
        int redShirtHeight = redShirtHeights[i];
        int blueShirtHeight = blueShirtHeights[i];
        if (shirtColorInFirstRow == "RED") {
            if (redShirtHeight >= blueShirtHeight) {
                return false;
            }
        } else if (blueShirtHeight >= redShirtHeight) {
            return false;
        }
    }

    return true;
}

int main() {
    std::vector<int> red{5, 8, 1, 3, 4};                                // 1st row
    std::vector<int> blue{6, 9, 2, 4, 5};                               // 2nd row

    std::cout << std::boolalpha << classPhotos(red, blue) << std::endl; // true


    
    return 0;
}
