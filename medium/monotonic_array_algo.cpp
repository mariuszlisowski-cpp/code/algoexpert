#include <cassert>
#include <vector>

// O(n) time | O(1) space, n - array length
bool isMonotonic(std::vector<int> array) {
    bool is_non_decreasing{ true };
    bool is_non_increasing{ true };
    for (int i = 1; i < array.size(); ++i) {
        if (array[i - 1] > array[i]) {
            is_non_decreasing = false;
        }
        if (array[i - 1] < array[i]) {
            is_non_increasing = false;
        }
    }

    return is_non_decreasing || is_non_increasing;
}

int main() {
    std::vector<int> non_decreasing{1, 5, 10, 11, 11, 15, 19};
    bool expected = true;
    bool actual = isMonotonic(non_decreasing);
    assert(expected == actual);

    std::vector<int> non_increasing{-1, -5, -10, -11, -11, -15, -19};
    expected = true;
    actual = isMonotonic(non_increasing);
    assert(expected == actual);

    std::vector<int> constant{2, 2, 2, 2, 2};
    expected = true;
    actual = isMonotonic(constant);
    assert(expected == actual);

    return 0;
}
