/*  g++ -std=c++17 -lgtest -lgtest_main array_of_products.cpp 

    Write a function that takes in a non-empty array of integers and returns an array of the same length,
    where each element in the output array is equal to the product of every other number in the input array.

    In other words, the value at output[i] is equal to the product
    of every number in the input array other than input[i].

    Note that you're expected to solve this problem without using division.
*/
#include <cstddef>
#include <gtest/gtest.h>
#include <vector>

/* time O(n); space O(n) */
auto arrayOfProducts_best(std::vector<int> array)
{
    std::vector<int> products(array.size());
    unsigned long left_running_product{ 1 };
    for (size_t i = 0; i < array.size(); ++i) {
        products[i] = left_running_product;
        left_running_product *= array[i];
    }
    unsigned long right_running_product{ 1 };
    for (size_t i = array.size() - 1; i + 1 > 0; --i ) {
        products[i] *= right_running_product;
        right_running_product *= array[i];
    }

    return products;
}

/*  left   1, 5, 5, 20          
           *  *  *  *
    right  8, 8, 2, 1
           =  =  =  =
    products 8 40 10 20
time O(n); space O(n) */
auto arrayOfProducts_optimal(std::vector<int> array)
{
    unsigned long left_running_product{ 1 };
    std::vector<int> left(array.size());
    for (size_t i = 0; i < array.size(); ++i) {
        left[i] = left_running_product;                                          // insert from begin
        left_running_product *= array[i];
    }
    unsigned long right_running_product = 1;
    std::vector<int> right(array.size());
    for (size_t i = array.size() - 1; i + 1 > 0; --i ) {
        right[i] = right_running_product;                                         // insert from end
        right_running_product *= array[i];
    }
    std::vector<int> products(array.size());
    for (size_t i = 0; i < array.size(); ++i) {
        products[i] = left[i] * right[i];
    }

    return products;
}

/* time O(n^2) */
auto arrayOfProducts_naive(std::vector<int> array)
{
    unsigned long left_running_product;
    std::vector<int> products;
    for (size_t i = 0; i < array.size(); ++i) {
        left_running_product = 1;
        for (size_t j = 0; j < array.size(); ++j) {
            if (j != i) {
                left_running_product *= array[j];
            }
        }
        products.push_back(left_running_product);
    }
    return products;
}
/*
  output[0] = [x] * 1 *  4 *  2 = 8;
  output[1] =  5 * [x] * 4 *  2 = 40;
  output[2] =  5 *  1 * [x] * 2 = 10;
  output[4] =  5 *  1 *  4 * [x] = 20;
*/
TEST(ArrayOfProductsTest,
     ProductOfVectorElements_ExcludingCurrentIndex_WithPositives)
{
    std::vector<int> input = {5, 1, 4, 2};
    std::vector<int> expected = {8, 40, 10, 20};
    EXPECT_EQ(arrayOfProducts_best(input), expected);
    EXPECT_EQ(arrayOfProducts_optimal(input), expected);
    EXPECT_EQ(arrayOfProducts_naive(input), expected);
}

TEST(ArrayOfProductsTest,
     ProductOfVectorElements_ExcludingCurrentIndex_WithNegatives)
{
    std::vector<int> input = {-1, -1, -1};
    std::vector<int> expected = {-1 * -1, -1 * -1, -1 * -1};
    EXPECT_EQ(arrayOfProducts_best(input), expected);
    EXPECT_EQ(arrayOfProducts_optimal(input), expected);
    EXPECT_EQ(arrayOfProducts_naive(input), expected);
}

/*
  output[0] = [x] * 1 *  4 *  2 = 8;
  output[1] =  0 * [x] * 4 *  2 = 0;
  output[2] =  0 *  1 * [x] * 2 = 0;
  output[4] =  0 *  1 *  4 * [x] = 0;
*/
TEST(ArrayOfProductsTest,
     ProductOfVectorElements_ExcludingCurrentIndex_WithOneZero)
{
    std::vector<int> input = {0, 1, 4, 2};
    std::vector<int> expected = {8, 0, 0, 0};
    EXPECT_EQ(arrayOfProducts_best(input), expected);
    EXPECT_EQ(arrayOfProducts_optimal(input), expected);
    EXPECT_EQ(arrayOfProducts_naive(input), expected);
}

/*
  output[0] = [x] * 1 *  0 *  0 = 0;
  output[1] =  0 * [x] * 0 *  0 = 0;
  output[2] =  0 *  1 * [x] * 0 = 0;
  output[4] =  0 *  1 *  0 * [x] = 0;
*/
TEST(ArrayOfProductsTest,
     ProductOfVectorElements_ExcludingCurrentIndex_WithTwoZeros)
{
    std::vector<int> input = {0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<int> expected = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    EXPECT_EQ(arrayOfProducts_best(input), expected);
    EXPECT_EQ(arrayOfProducts_optimal(input), expected);
    EXPECT_EQ(arrayOfProducts_naive(input), expected);
}

TEST(ArrayOfProductsTest,
     ProductOfVectorElements_ExcludingCurrentIndex_WithMultipleZeros)
{
    std::vector<int> input = {0, 1, 0, 0};
    std::vector<int> expected = {0, 0, 0, 0};
    EXPECT_EQ(arrayOfProducts_best(input), expected);
    EXPECT_EQ(arrayOfProducts_optimal(input), expected);
    EXPECT_EQ(arrayOfProducts_naive(input), expected);
}
