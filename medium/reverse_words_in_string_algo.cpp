#include <string>
#include <vector>

void reverseList(std::vector<std::string>& list);

// O(n) time | O(n) space
std::string reverseWordsInString(std::string str) {
    std::vector<std::string> words;
    int startOfWord = 0;
    for (int idx = 0; idx < str.size(); idx++) {
        char character = str[idx];

        if (character == ' ') {
            words.push_back(str.substr(startOfWord, idx - startOfWord));
            startOfWord = idx;
        } else if (str[startOfWord] == ' ') {
            words.push_back(" ");
            startOfWord = idx;
        }
    }

    words.push_back(str.substr(startOfWord));

    reverseList(words);
    std::string output;
    
    for (auto& word : words) {
        output += word;
    }
    return output;
}

void reverseList(std::vector<std::string>& list) {
    int start = 0;
    int end = list.size() - 1;
    while (start < end) {
        std::string temp = list[start];
        list[start] = list[end];
        list[end] = temp;
        start++;
        end--;
    }
}

int main() {
    std::string input = "AlgoExpert  is the best!";
    std::string expected = "best! the is  AlgoExpert";

    auto actual = reverseWordsInString(input);
    assert(expected == actual);
}
