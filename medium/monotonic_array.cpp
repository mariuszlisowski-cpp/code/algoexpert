/*  Ciągi monotoniczne:
    Ciąg rosnący     – każdy kolejny wyraz jest większy od poprzedniego
                       2,4,6,8,10…   5,10,15,20,25…   -3,−2,−1,0,1…
    Ciąg malejący    – każdy kolejny wyraz jest mniejszy od poprzedniego
                       100,90,80,70,60…   3,2,1,0,−1…   −15,−16,−17,−18,−19…
    Ciąg stały       – każdy kolejny wyraz jest taki saa jak poprzedni
                       2,2,2,2,2…   5,5,5,5,5…
    Ciąg niemalejący – ciąg w którym każdy kolejny wyraz jest większy lub równy poprzedniemu
                       1,1,2,2,3…   2,7,7,8,9…
    Ciąg nierosnący  – ciąg w którym każdy kolejny wyraz jest mniejszy lub równy poprzedniemu
                       4,4,3,3,2…   10,10,10,9,7…
*/

#include <cassert>
#include <vector>

#include <iostream>

// O(n) time | O(1) space, n - array length
bool isMonotonic(std::vector<int> array) {
    if (array.size() > 1) {
        bool is_non_decreasing_suspect{};
        if (array.front() < array.back()) {
            is_non_decreasing_suspect = true;
        }
        // std::cout << std::boolalpha << is_non_decreasing_suspect << std::endl;
        using array_it = std::vector<int>::const_iterator;
        for (array_it it_first{ array.cbegin() }, it_second{ it_first + 1 };
             it_second != array.cend(); ++it_first, ++it_second)
        {
            if (is_non_decreasing_suspect) {
                // std::cout << "non-de" << std::endl;
                if (*it_first > *it_second) {                                // non-decreasing check
                    return false;
                }
            } else {
                // std::cout << "non-in" << std::endl;
                if (*it_first < *it_second) {                                // non-increasing check
                    return false;
                }
            }
        }
    }

    return true;                                                             // empty or with one element are monotonic
}

int main() {
    std::vector<int> non_monotonic{-1, -5, -10, -10, 1, -11};
    bool expected = false;
    bool actual = isMonotonic(non_monotonic);
    assert(expected == actual);

    std::vector<int> monotonic = {1, 5, 10, 11, 11, 15};
    expected = true;
    actual = isMonotonic(monotonic);
    assert(expected == actual);

    std::vector<int> constant_monotonic{2, 2, 2, 2, 2};
    expected = true;
    actual = isMonotonic(constant_monotonic);
    assert(expected == actual);

    return 0;
}
