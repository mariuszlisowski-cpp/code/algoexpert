#include <cassert>
#include <vector>

int longestPeak(std::vector<int> array) {
    int longest_peak_length{};
        if (!array.empty()) {
        for (int i = 1; i < array.size() - 1; ++i) {
            if (array[i - 1] < array[i] && array[i] > array[i + 1]) {
                int left = i - 1;
                while (left > 0 && array[left ] > array[left - 1]) {
                    --left;
                }

                int right = i + 1;
                while (right < array.size() - 1 && array[right] > array[right + 1]) {
                    ++right;
                }

                auto peak_length{ right - left + 1};
                longest_peak_length = std::max(longest_peak_length, peak_length);
            }
        }
    }

    return longest_peak_length;
}

int main() {
    std::vector<int> input = { 1, 2, 3, 3, 4, 0, 10, 6, 5, -1, -3, 2, 3 };
    int expected = 6;                      // 0, 10, 6, 5, -1, -3
    int actual = longestPeak(input);
    
    assert(expected == actual);

    return 0;
}
