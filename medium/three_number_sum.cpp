#include <algorithm>
#include <cassert>
#include <vector>

// O(n^2) time | O(n) space
std::vector<std::vector<int>> threeNumberSum(std::vector<int> array, int targetSum) {
    std::sort(array.begin(), array.end());

    std::vector<std::vector<int>> triplets;
    auto current{ array.begin() };                                                  // start from the left
    do {
        auto left{ current + 1};                                                    // set left
        auto right{ array.end() - 1 };                                              // ..and right pointer
        while (left < right) {
            int sum{ *current + *left + *right };
            if (targetSum == sum) {
                triplets.push_back(std::vector<int>{ *current, *left, *right });    // found!
                ++left;                                                             // bigger
                --right;                                                            // ..or smaller?
            } else {
                if (sum < targetSum) {                                              // get a bigger sum than target
                    ++left;
                } else {                                                            // get a smaller sum than target
                    --right;
                }
            }
        }
        ++current;
    } while (current != array.end() - 2);                                           // break if two numbers left
                                                                                    // ..because same left & right
    return triplets;
}


int main() {
    std::vector<std::vector<int>> expected{
        {-8, 2, 6},
        {-8, 3, 5},
        {-6, 1, 5}
    };

    assert(threeNumberSum({12, 3, 1, 2, -6, 5, -8, 6}, 0) == expected);    

    return 0;
}
