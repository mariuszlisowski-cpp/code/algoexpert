#include <string>
#include <vector>
#include <iostream>
#include <iomanip>

// O(n) time | O(n) space
std::string reverseWordsInString(std::string str) {
    std::vector<std::string> words;
    int startOfWord = 0;
    for (int idx = 0; idx < str.size(); idx++) {
        char character = str[idx];
        if (character == ' ') {
            auto word{str.substr(startOfWord, idx - startOfWord)};
            std::cout << std::quoted(word) << std::endl;                        // verbose
            words.push_back(word);
            startOfWord = idx;
        } else if (str[startOfWord] == ' ') {
            auto space{" "};
            std::cout << std::quoted(space) << " space before" << std::endl;    // verbose
            words.push_back(space);
            startOfWord = idx;
        }
    }
    auto last{str.substr(startOfWord)};
    std::cout << last << std::endl;                                             // verbose
    words.push_back(last);

    std::string output;
    for (auto word = words.rbegin(); word != words.rend(); ++word) {
        output += *word;
    }

    return output;
}

int main() {
    std::string input = "AlgoExpert   is the best!";
    std::string expected = "best! the is   AlgoExpert";

    auto actual = reverseWordsInString(input);
    assert(expected == actual);
}
