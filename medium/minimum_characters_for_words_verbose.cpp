#include <algorithm>
#include <cassert>
#include <iterator>
#include <string>
#include <unordered_map>
#include <vector>

#include <iostream>

std::vector<char> minimumCharactersForWords(std::vector<std::string> words) {
    std::unordered_map<char, unsigned> occurences;
    std::unordered_map<char, unsigned> results;
    for (const auto& word : words) {
        for (const auto& ch : word) {
            ++occurences[ch];
        }
        for (auto [ch, occurence] : occurences) {
            std::cout << ch << ':' << occurence << ' ';
        }
        std::cout << std::endl;
        std::for_each(occurences.begin(), occurences.end(),
                       [&](auto& occurence) {
                           if (results.count(occurence.first) == 0 ||
                               results[occurence.first] < occurence.second)
                           {
                               results[occurence.first] = occurence.second;
                           }
                       });
        occurences.clear();
    }
    
    for (auto [ch, result] : results) {
        std::cout << "# " << ch << ' ' << result << std::endl;
    }
    std::cout << std::endl;

    std::vector<char> output;
    std::for_each(results.begin(), results.end(),
                  [&](const auto& result) {
                      for (auto i{0}; i < result.second; ++i) {
                          output.push_back(result.first);
                      }
                  });

    return output;
}

int main() {
    {
    std::vector<std::string> input = {"this", "that", "them"};
    std::vector<char> expected = {'t', 't', 'h', 'i', 's', 'a', 'e', 'm'};
    // t1 h1 i1 s1  |  t2 h1 a1  |  t1 h1 e1 m1 -> t2 h1 i1 s1 a1 e1 m1
    //     ^  ^  ^      ^     ^            ^  ^
    
    auto actual = minimumCharactersForWords(input);
    std::sort(actual.begin(), actual.end());
    std::sort(expected.begin(), expected.end());
    
    assert(expected == actual);
    }
    
    {
    std::vector<std::string> input = {"this", "that", "did", "deed", "them!", "a"};
    std::vector<char> expected = {'t', 't', 'h', 'i', 's', 'a', 'd', 'd', 'e', 'e', 'm', '!'};

    auto actual = minimumCharactersForWords(input);
    std::sort(actual.begin(), actual.end());
    std::sort(expected.begin(), expected.end());
    
    assert(expected == actual);
    }
 
    {
    std::vector<std::string> input = {"!!!2", "234", "222", "432"};
    std::vector<char> expected = {'2', '2', '2', '!', '!', '!', '3', '4'};
    
    auto actual = minimumCharactersForWords(input);
    std::sort(actual.begin(), actual.end());
    std::sort(expected.begin(), expected.end());
    
    assert(expected == actual);
    }
}

